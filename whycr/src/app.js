const registerSW = async () => {
  if ("serviceWorker" in navigator) {
    try {
      const reg = await navigator.serviceWorker.register("/sw.js", {
        scope: "/",
      });

      if (reg.installing) {
        console.log("Installing");
      } else if (reg.waiting) {
        console.log("Installed");
      } else if (reg.active) {
        console.log("Active");
      }
    } catch (err) {
      console.log(`Registration failed with ${err}`);
    }
  }
};

registerSW();
