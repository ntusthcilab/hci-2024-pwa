import { html, css, LitElement } from 'lit';

import '@material/web/button/filled-button';
import '@material/web/button/filled-tonal-button';
import '@material/web/textfield/outlined-text-field';


class Editor extends LitElement {
  static styles = css`
    :host > * {
      width: fit-content;
      margin: 0 auto;
    }

    md-outlined-text-field {
      margin-bottom: 1em;
    }
  `;

  static properties = {
    idx: {type: Number},
    link: {type: String},
  }

  constructor() {
    super();
    this.idx = 0;
    this.link = '';

    this.handleMember = null;
    this.handlePage = null;
  }

  _onSubmit(event) {
    event.preventDefault();

    let form = new FormData(event.target);
    let data = {
      idx: Number(form.get('idx')),
      link: form.get('link'),
    };

    this.handleMember(data);

    this.renderRoot.querySelector('form').reset();
    this.handlePage(null);
  }

  _onCancel() {
    this.renderRoot.querySelector('form').reset();
    this.handlePage(null);
  }

  render() {
    return html`
    <form @submit=${this._onSubmit}>
      <input type="hidden" name="idx" value=${this.idx} />
      <div>
        <md-outlined-text-field
          label="URL"
          name="link"
          value=${this.link}
          required
        >
        </md-outlined-text-field>
      </div>
      <md-filled-button type="submit">Submit</md-filled-button>
      <md-filled-tonal-button type="button" @click=${this._onCancel}>Cancel</<md-filled-tonal-button>
    </form>
    `;
  }
}


window.customElements.define('whycr-editor', Editor);
export default Editor;
