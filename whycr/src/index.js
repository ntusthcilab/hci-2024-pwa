import { html, LitElement } from 'lit';

import './list';
import './edit';


const DEFAULT_MEMBERS = [
    {idx: 1, title: 'HCI Lab', link: 'http://dianalab.net', date: '2024/4/23'},
    {idx: 2, title: 'Nice Music', link: 'https://youtu.be/dQw4w9WgXcQ', date: '2024/4/23'},
];

let loadDefaultMembers = true;


class App extends LitElement {
  static properties = {
    page: {state: true},
  }

  constructor() {
    super();
    this.members = this.loadStorage();
    this.page = null;
  }

  saveStorage() {
    window.localStorage.setItem('members', JSON.stringify(this.members));
  }

  loadStorage() {
    if (!window.localStorage.getItem('members')) {
      if (loadDefaultMembers) {
        window.localStorage.setItem('members', JSON.stringify(DEFAULT_MEMBERS))
      } else {
        window.localStorage.setItem('members', JSON.stringify([]))
      }
    }
    return JSON.parse(window.localStorage.getItem('members'));
  }

  _updatePage(page) {
    this.page = page;
  }

  _updateMember({idx, link}) {
    if (idx > 0) {
      for (let i = 0; i < this.members.length; ++i) {
        if (this.members[i].idx === idx) {
          this.members[i].link = link;
        }
      }
    } else {
      let now = new Date();
      this.members.push({
        idx: (this.members.length > 0) ? this.members[this.members.length - 1].idx + 1 : 1,
        link: link,
        date: `${now.getFullYear()}/${now.getMonth() + 1}/${now.getDate()}`,
      })
    }
    window.localStorage.setItem('members', JSON.stringify(this.members));
  }

  renderList() {
    return html`
    <whycr-itemlist
      .handlePage=${this._updatePage.bind(this)}
      .members=${this.members}/>
    `;
  }

  renderEditor({idx, link}) {
    return html`
    <whycr-editor
      .handlePage=${this._updatePage.bind(this)}
      .handleMember=${this._updateMember.bind(this)}

      idx=${idx}
      link=${link}
      />
    `;
  }

  render() {
    if (window.location.hash === '#new') {
      this._updatePage('new');
    }

    if (this.page === 'new') {
      return this.renderEditor({});
    } else if (!!this.page) {
      return this.renderEditor(this.page);
    } else {
      return this.renderList();
    }
  }
}


window.customElements.define('whycr-app', App);
