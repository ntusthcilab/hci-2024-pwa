import { html, LitElement } from 'lit';

import '@material/web/button/filled-button'
import '@material/web/dialog/dialog';
import '@material/web/divider/divider';
import '@material/web/icon/icon';
import '@material/web/iconbutton/icon-button';
import '@material/web/list/list-item';
import '@material/web/list/list';
import '@material/web/menu/menu-item';
import '@material/web/menu/menu';


class Item extends LitElement {
  static properties = {
    idx: {type: Number},
    link: {type: String},
    date: {type: String},
  };

  constructor() {
    super();
    this.idx = 0;
    this.link = '';
    this.date = '';

    this.handlePage = null;
  }

  _onTapCode() {
    this.renderRoot.querySelector('md-dialog').open = true;
  }

  renderCode() {
    return html`
    <md-menu-item @click=${this._onTapCode}>
      <md-icon slot="start">qr_code_2</md-icon>
      <div slot="headline">Show Code</div>
    </md-menu-item>
    `;
  }

  _onCloseCodeBlock() {
    this.renderRoot.querySelector('md-dialog').open = false;
  }

  renderCodeBlock() {
    return html`<md-dialog>
      <div slot="content">
        <img
          width="100%"
          src="https://api.qrserver.com/v1/create-qr-code/?size=400x400&data=${this.link}&qzone=4"
          />
      </div>
    </md-dialog>
    `;
  }

  _onTapBrowse() {
    window.open(this.link, "new-window");
  }

  renderBrowse() {
    return html`
    <md-menu-item @click=${this._onTapBrowse}>
      <md-icon slot="start">open_in_new</md-icon>
      <div slot="headline">Open</div>
    </md-menu-item>
    `;
  }

  _onTapEdit() {
    this.handlePage({
      idx: this.idx,
      link: this.link,
      date: this.date,
    })
  }

  renderEdit() {
    return html`
    <md-menu-item @click=${this._onTapEdit}>
      <md-icon slot="start">edit</md-icon>
      <div slot="headline">Edit</div>
    </md-menu-item>
    `;
  }

  renderDelete() {
    return html`
    <md-menu-item>
      <md-icon slot="start">delete</md-icon>
      <div slot="headline">Delete</div>
    </md-menu-item>
    `;
  }

  _onTapMenu() {
    let opened = this.renderRoot.querySelector('md-menu').open;
    this.renderRoot.querySelector('md-menu').open = !opened;
  }

  render() {
    return html`
    <md-list-item>
      <div slot="supporting-text">${this.link}</div>
      <div slot="supporting-text">Created at: ${this.date}</div>
      <div slot="end">
        <md-icon-button id="more" @click=${this._onTapMenu}>
          <md-icon>more_vert</md-icon>
        </md-icon-button>
      </div>
      <md-menu has-overflow anchor="more"  positioning="popover">
        ${this.renderCode()}
        ${this.renderBrowse()}
        ${this.renderEdit()}
      </md-menu>
      ${this.renderCodeBlock()}
    </md-list-item>`;
  }
}

window.customElements.define('whycr-item', Item);


export class ItemList extends LitElement {
  static properties = {
    members: {
      type: Array,
    },
  };

  constructor() {
    super();
    this.members = []
    this.handlePage = null;
  }

  renderItem({ idx, date, link, ...rest }) {
    return html`
    <whycr-item
      idx=${idx}
      link=${link}
      date=${date}

      .handlePage=${this.handlePage}
      />
    `;
  }

  _onTapCreate() {
    this.handlePage('new');
  }

  render() {
    let body;
    if (this.members.length === 0) {
      body = html`<p>Empty...</p>`
    } else {
      body = html`
      <md-list>
        ${
          this.members.map((data, i) => html`
            ${this.renderItem(data)}
            ${(i < this.members.length - 1) ? html`<md-divider></md-divider>` : null}
            `)
        }
      </md-list>`;
    }
    return html`
    ${body}
    <div id="button-container">
      <md-filled-button @click=${this._onTapCreate}>
        <md-icon slot="icon">add</md-icon>
        Add
      </md-filled-button>
    </div>
    `;
  }
}


window.customElements.define('whycr-itemlist', ItemList);
export default ItemList;
