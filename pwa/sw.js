const VERSION = "v1";
const CACHE_NAME = `hello-${VERSION}`;

// Static resource
const APP_STATIC_RESOURCES = [
  "/",
  "/index.html",
  // TODO: Add static resource for caching here
];

// Cache resources on installation
self.addEventListener("install", (event) => {
  console.log('SW: On installation');

  event.waitUntil(
    (async () => {
      const cache = await caches.open(CACHE_NAME);
      cache.addAll(APP_STATIC_RESOURCES);
    })()
  );
});

// Drop previous caches on activation
self.addEventListener("activate", (event) => {
  console.log('SW: On activation');

  event.waitUntil(
    (async () => {
      const names = await caches.keys();
      await Promise.all(
        names.map((name) => {
          if (name !== CACHE_NAME) {
            return caches.delete(name);
          }
        })
      );
      await clients.claim();
    })()
  );
});

self.addEventListener("fetch", (event) => {
  console.log('SW: On fetch');

  if (event.request.mode === "navigate") {
    event.respondWith(caches.match("/"));
    return;
  }

  event.respondWith(
    (async () => {
      const cache = await caches.open(CACHE_NAME);
      const cached = await cache.match(event.request);
      if (cached) {
        return cached;
      }
      return fetch(event.request);
    })()
  );
});
